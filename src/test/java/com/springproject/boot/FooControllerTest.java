package com.springproject.boot;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc//inject a MockMvc instance and send HTTP request
public class FooControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void test_app_get() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/foos"))
                    .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    public void test_app_post() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.post("/foos"))
                    .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }
}
