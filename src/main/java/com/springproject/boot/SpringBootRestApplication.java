package com.springproject.boot;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication// equivalent to using @Configuration., @EnableAutoConfiguration, @ComponentScan
//by default will scan the components in the same package or below
public class SpringBootRestApplication {
}
