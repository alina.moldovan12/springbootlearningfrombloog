package com.springproject.boot;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

// this is the main artifact used by Java based spring configuration , it is istself annotated with @Component , which makes the annotated classes standard beans , also candidates for component scanning
//the main purpose of @Configuration classes is to be sources of bean definitions for the Spring IoC Container
@Configuration//config class of Spring beans(Jav based spring configuration)
@EnableWebMvc//it detects the existance of jackson and jaxb2 on the classpath and automatically creates and registers default json and xml converters
@ComponentScan(basePackages = "com.springproject.boot")
//The @EnableWebMvc annotation provides the Spring Web MVC configuration such as setting up the dispatcher servlet, enabling the @Controller and the @RequestMapping  annotations and setting up other defaults.
//@ComponentScan configures the component scanning directive, specifying the packages to scan.
public class WebConfig {

}
